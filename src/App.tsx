import React, { FC } from "react";
import { MineSweeper } from "./Minesweeper/MineSweeper";
import { ThemeProvider } from "./Minesweeper/Themes";

export const App: FC = () => (
  <ThemeProvider>
    <MineSweeper />
  </ThemeProvider>
);
